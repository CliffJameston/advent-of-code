# Prep
rounds = []

# Read in file, totaling and splitting inventories at each blank line
with open('inputs/day02.txt') as input:
    for line in input:
        rounds.append(line.strip())

def roundResult(theirPlay, yourPlay):
    theirScore = interpret(theirPlay)
    yourScore = interpret(yourPlay)

    if(theirScore == (yourScore-1) or theirScore == (yourScore+2)):
        # You win! Score 6
        return 6
    elif(theirScore == (yourScore+1) or theirScore == (yourScore-2)):
        # You lose. Score 0
        return 0
    else:
        # Tie. Score 3
        return 3

def interpret(play):
    match play:
        case "A" | "X":
            return 1
        case "B" | "Y":
            return 2
        case "C" | "Z":
            return 3
    return 0

def interpretPlay(code, theirPlay):
    # X = Lose, Y = Draw, Z = Win
    # This turned out really stupid
    if((theirPlay == "B" and code == "X") or (theirPlay == "A" and code == "Y") or (theirPlay == "C" and code == "Z")):
        return "X"
    if((theirPlay == "C" and code == "X") or (theirPlay == "B" and code == "Y") or (theirPlay == "A" and code == "Z")):
        return "Y"
    if((theirPlay == "A" and code == "X") or (theirPlay == "C" and code == "Y") or (theirPlay == "B" and code == "Z")):
        return "Z"

def Part1():
    totalScore = 0
    # Scores: X = 1, Y = 2, Z = 3
    # Loss = 0, Tie = 3, Win = 6
    for round in rounds:
        theirPlay = round[0]
        yourPlay = round[2]
        # Add score for your play
        totalScore += interpret(yourPlay)
        # Add score for result
        totalScore += roundResult(theirPlay, yourPlay)
    return totalScore

def Part2():
    totalScore = 0

    for round in rounds:
        theirPlay = round[0]
        yourPlay = interpretPlay(round[2], theirPlay)
        # Add score for your play
        totalScore += interpret(yourPlay)
        # Add score for result
        totalScore += roundResult(theirPlay, yourPlay)
    return totalScore

print("-----\nPart 1\n-----\n")
print("Total Score: " + str(Part1()))

print()
print("-----\nPart 2\n-----\n")
print("Total Score: " + str(Part2()))