from Common import Helper

filePath = 'inputs/day03.txt'

def getValue(letter):
    if(str(letter).isupper()):
        return ord(letter)-38
    else:
        return ord(letter)-96

def getCompartmentMatch(inventoryString):
    split1 = set(inventoryString[slice(0,len(inventoryString)//2)])
    split2 = set(inventoryString[slice(len(inventoryString)//2,len(inventoryString))])
    return set(split1).intersection(split2)

def Part1(inventory):
    prioritySum = 0
    for line in inventory:
        for match in getCompartmentMatch(line):
            prioritySum += getValue(match)
    return prioritySum

def Part2(inventory):
    prioritySum = 0
    group = []
    for index, line in enumerate(inventory, 1):
        group.append(line.strip())
        if((index) % 3 == 0):
            for match in set.intersection(set(group[0]), set(group[1]), set(group[2])):
                prioritySum += getValue(match)
            group.clear()
    return prioritySum

print("-----\nPart 1\n-----\n")
print(Part1(Helper.process(filePath)))

print()
print("-----\nPart 2\n-----\n")
print(Part2(Helper.process(filePath)))