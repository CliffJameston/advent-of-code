from Common import Helper

filePath = 'inputs/day.txt'
puzzleInput = Helper.process(filePath)

def Part1(input):
    return

def Part2(input):
    return

print("-----\nPart 1\n-----\n")
print(Part1(puzzleInput))

print()
print("-----\nPart 2\n-----\n")
print(Part2(puzzleInput))