# File input: each line contains either an int, or blank
# Int represents one item's worth of calories in an inventory
# Blank represents a break between inventories
# (Eg. One inventory is all ints between two blank lines)

# Find highest Inventory total

# Prep
inventories = [0]

# Read in file, totaling and splitting inventories at each blank line
with open('inputs/day01.txt') as input:
    pointer = 0
    for line in input:
        if(line == "\n"):
            inventories.append(0)
            pointer += 1
        else:
            inventories[pointer] += int(line)

def Part1():
    largestInventory = 0
    index = 0

    # Run through each inventory, saving the largest.
    for current, inventory in enumerate(inventories, start=1):
        if(inventory>largestInventory):
            index = current
            largestInventory = inventory
            # Debugging: Print each largest inventory
            #print(str(pointer) + " - " + str(largest))
    print("Largest: " + str(largestInventory) + "\nIndex: " + str(index))
    return largestInventory

def Part2():
    firstLargest = 0
    secondLargest = 0
    thirdLargest = 0
    total = 0
    # Run through each inventory, saving each of the top 3 largest.
    for inventory in inventories:
        if(inventory>firstLargest):
            thirdLargest = secondLargest
            secondLargest = firstLargest
            firstLargest = inventory
        elif(inventory>secondLargest):
            thirdLargest = secondLargest
            secondLargest = inventory
        elif(inventory>thirdLargest):
            thirdLargest = inventory
    total = firstLargest+secondLargest+thirdLargest
    print(str(firstLargest) + " + " + str(secondLargest) + " + " + str(thirdLargest) + " = " + str(total) + "\n")
    return total

print("-----\nPart 1\n-----\n")
Part1()

print()
print("-----\nPart 2\n-----\n")
Part2()