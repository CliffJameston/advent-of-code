from Common import Helper

filePath = 'inputs/day04.txt'
puzzleInput = Helper.process(filePath)
def processPairs(pairLine):
    pairs = pairLine.split(',')
    return pairs

def stringToSet(setString):
    ends = setString.split('-')
    fullSet = set(range(int(ends[0]), int(ends[1])+1))
    return fullSet

def Part1(input):
    fullyContained = 0
    for line in input:
        splitLine = processPairs(line)
        setOne = stringToSet(splitLine[0])
        setTwo = stringToSet(splitLine[1])
        if(setOne.issubset(setTwo) or setTwo.issubset(setOne)):
            fullyContained += 1
    return fullyContained

def Part2(input):
    intersect = 0
    for line in input:
        splitLine = processPairs(line)
        setOne = stringToSet(splitLine[0])
        setTwo = stringToSet(splitLine[1])
        setIntersection = set.intersection(setOne,setTwo)
        if(len(setIntersection) != 0):
            intersect += 1
    return intersect

print("-----\nPart 1\n-----\n")
print(Part1(puzzleInput))

print()
print("-----\nPart 2\n-----\n")
print(Part2(puzzleInput))