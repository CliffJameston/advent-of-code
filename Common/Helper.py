def process(fileLocation):
    processedFile = []
    with open(fileLocation) as input:
        for line in input:
            processedFile.append(line.strip())
    return processedFile